// import {browser, element, by , protractor, $$, $} from 'protractor'; 
let helper = require('../helper/helper.js');
let util = require('../helper/util.js');


let loginPOM = function(){

    let usernameInput = element(by.model('Email'));
    let passwordInput =  element(by.model('Password'))
    let signInBtn =  element(by.id('enterbtn'))
    let errormsgTxt =  element(by.id('errormsg'))

    this.getURL = function(url){
        browser.driver.get(url);
    }

    this.enterUsernameAndPass = function(email,password){
        util.utilElementSendText(usernameInput, email);
        util.utilElementSendText(passwordInput, password);
    }

    this.clickSignInBtn = function(){
        util.utilElementClick(signInBtn);
    };


    this.checkIfErrorMessagePresent = function(){
        helper.waitForElementPresent(errormsgTxt);
        helper.waitForElementDisplayed(errormsgTxt);
        return errormsgTxt.getText();
    }
};

module.exports = new loginPOM();