let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let alertPOM = function () {

    let alertOKTab = element(by.linkText('Alert with OK'));
    let alertOKCancelTab = element(by.linkText('Alert with OK & Cancel'));
    let alertTextBoxTab = element(by.linkText('Alert with Textbox'));
     
    let alertOKCancelBtn = element(by.buttonText('click the button to display a confirm box'));
    let alertTextboxBtn = element(by.buttonText('click the button to demonstrate the prompt box'));
     
    let confirmmsgTxt = element(by.id('demo'));
    let greetingmsgTxt = element(by.id('demo1'));
    
    this.selectAlertOKCancelTab = function () {
        alertOKCancelTab.click();
    }

    this.selectAlertWithTextbox = function () {
        alertTextBoxTab.click();
    }

    this.selectAlertWithTextboxBtn = function () {
        alertTextboxBtn.click();
    }

    this.enterIntoTextbox = function () {

    }

    this.selectAlertOKCancelBtn = function () {
        alertOKCancelBtn.click();
    }

    this.checkIfMessagePresent = function () {
        helper.waitForElementPresent(confirmmsgTxt);
        helper.waitForElementDisplayed(confirmmsgTxt);
        return confirmmsgTxt.getText();
    }

    this.checkIfGreetingMessagePresent = function () {
        helper.waitForElementPresent(greetingmsgTxt);
        helper.waitForElementDisplayed(greetingmsgTxt);
        return greetingmsgTxt.getText();
    }
};

module.exports = new alertPOM();