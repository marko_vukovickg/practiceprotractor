let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let calculatorPOM = function () {

    let firstNumberInput = element(by.model('first'))
    let secondNumberInput = element(by.model('second'))
    let operatorButton = element(by.model('operator'))
    let submitButton = element(by.buttonText('Go!'))
    let output = element(by.cssContainingText('.ng-binding', '2'))

    this.choseNumbers = function (firstNumber, secondNumber) {
        util.utilElementClick(firstNumberInput);
        util.utilElementSendText(firstNumberInput, firstNumber);

        util.utilElementClick(secondNumberInput);
        util.utilElementSendText(secondNumberInput, secondNumber);
    }

    this.choseOperation = function (operation) {
        util.utilElementClick(operatorButton);
        helper.findElementByOptionText(operation).click();
    }

    this.choseSubmit = function () {
        util.utilElementClick(submitButton);
    }

    this.getResult = function (result) {
        return element(by.cssContainingText('.ng-binding', result)).getText();
    }
};

module.exports = new calculatorPOM();