let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let widgetsPOM = function () {

    let datePickerDisabled = element(by.id('datepicker1'));
    let datePickerEnabled = element(by.id('datepicker2'));
    let monthFieldDisPicker = $$('.ui-datepicker-month');
    let yearFieldDisPicker = $$('.ui-datepicker-year');
    let nextFieldDisPicker = $$('.ui-datepicker-next');
    var li = element(by.xpath("//span[contains(text(),'Next')]"));
    let prevFieldDisPicker = $$('.ui-datepicker-prev');
    let searchBox = element(by.id('searchbox'));


    this.enterSearchData = function (searchData) {
        util.utilElementSendText(searchBox, searchData);
    }

    this.choseProvidedResult = function () {
        $$('.ui-autocomplete li').then(function (items) {
            items[0].click();
            browser.sleep(3000);
        })
    }

    this.selectDatePickerDisabled = function () {
        helper.waitForElementDisplayed(datePickerDisabled);
        helper.waitForElementPresent(datePickerDisabled);
        datePickerDisabled.click();
    }

    this.selectDatePickerEnabled = function () {
        helper.waitForElementDisplayed(datePickerEnabled);
        helper.waitForElementPresent(datePickerEnabled);
        datePickerEnabled.click();
    }

    this.enterDateOnDisabledDatePicker = function (date) {
        helper.waitForElementDisplayed(datePickerDisabled);
        helper.waitForElementPresent(datePickerDisabled);
        browser.executeScript("document.getElementById('datepicker1').value='" + date + "'");
    }

    this.enterDateOnEnabledDatePicker = function (date) {
        helper.waitForElementDisplayed(datePickerEnabled);
        helper.waitForElementPresent(datePickerEnabled);
        util.utilElementSendText(datePickerEnabled, date);
    }

    this.choseDateOnDisabledDatePicker = function (date) {
        var arr = date.split('/');
        this.enterMonthName(this.getMonthName(arr[0]), arr[2]);
        this.choseDay(arr[1], arr[0]);
    }

    this.choseDay = async function (day, month) {
        let chosenDay = day.split('');
        if (chosenDay[0] == "0") {
            day = chosenDay[1]
        }

        let chosenMonth = month.split('');
        if (chosenMonth[0] == "0") {
            month = chosenMonth[1]
        }

        console.log("Mesec: " + month + " Dan: " + day)

        var a = parseInt(month)
        var a = a - 1;

        a = a.toString();

        let fullDate = element(by.xpath("//td[@data-month=" + a + "]//a[contains(text()," + day + ")]"));
        await browser.sleep(3000);
        datePickerDisabled.click();
        await fullDate.click();
        await browser.sleep(4000);
    }


    this.getMonthName = function (monthNum) {
        var b;
        switch (monthNum) {
            case '01': b = "January";
                break;
            case '02': b = "February";
                break;
            case '03': b = "March";
                break;
            case '04': b = "April";
                break;
            case '05': b = "May";
                break;
            case '06': b = "June";
                break;
            case '07': b = "July";
                break;
            case '08': b = "August";
                break;
            case '09': b = "September";
                break;
            case '10': b = "October";
                break;
            case '11': b = "November";
                break;
            case '12': b = "December";
                break;
        }
        return b;
    }

    this.enterMonthName1 = async function (monthName, yearName) {
        helper.waitForElementDisplayed(monthFieldDisPicker);
        helper.waitForElementPresent(monthFieldDisPicker);

        let actualMonth
        let actualYear

        let monthVar = monthFieldDisPicker.getText();
        await monthVar.then(function (actual) {
            actualMonth = actual;
        });

        let yearVar = yearFieldDisPicker.getText();
        await yearVar.then(function (actual) {
            actualYear = actual;
        });

        while (actualMonth != monthName && actualYear != yearName) {
            let msg = monthFieldDisPicker.getText();
            await msg.then(function (actual) {
                actualMonth = actual;
            });

            let yearVar = yearFieldDisPicker.getText();
            await yearVar.then(function (actual) {
                actualYear = actual;
            });

            var EC = protractor.ExpectedConditions;
            await nextFieldDisPicker.click();
            browser.wait(EC.presenceOf(nextFieldDisPicker), 5000);
        }
        await prevFieldDisPicker.click();
    }

    this.enterMonthName = async function (monthName, yearName) {
        helper.waitForElementDisplayed(monthFieldDisPicker);
        helper.waitForElementPresent(monthFieldDisPicker);

        let actualMonth
        let actualYear

        let monthVar = monthFieldDisPicker.getText();
        await monthVar.then(function (actual) {
            actualMonth = actual;
        });

        let yearVar = yearFieldDisPicker.getText();
        await yearVar.then(function (actual) {
            actualYear = actual;
        });

        while (actualMonth != monthName || actualYear != yearName) {
            let msg = monthFieldDisPicker.getText();
            await msg.then(function (actual) {
                actualMonth = actual;
            });

            let yearVar = yearFieldDisPicker.getText();
            await yearVar.then(function (actual) {
                actualYear = actual;
            });

            var EC = protractor.ExpectedConditions;
            await nextFieldDisPicker.click();
            browser.wait(EC.presenceOf(nextFieldDisPicker), 5000);
        }
        await prevFieldDisPicker.click();
    }

};

module.exports = new widgetsPOM();