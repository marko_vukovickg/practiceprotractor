var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let widgetsPOM = require('../pages/widgetsPOM.js');


When('User selects date picker disabled widget', async function () {// 
    widgetsPOM.selectDatePickerDisabled();
    await browser.sleep(3000);
});


When('User selects date picker enabled widget', async function () {// 
    widgetsPOM.selectDatePickerEnabled();
    await browser.sleep(3000);
});


Then('User enters the date {string} on disabled widget', async function (date) {// 
    widgetsPOM.enterDateOnDisabledDatePicker(date);
    await browser.sleep(2000);
});


Then('User enters the date {string} on enabled widget', async function (date) {// 
    widgetsPOM.enterDateOnEnabledDatePicker(date);
    await browser.sleep(2000);
});


Then('User chose the date {string} on disabled widget', async function (date) {// 
    widgetsPOM.choseDateOnDisabledDatePicker(date);
    await browser.sleep(4000);
});


Then('User enters search parameter {string}', async function (searchData) {// 
    widgetsPOM.enterSearchData(searchData);
    await browser.sleep(4000);
});


Then('User chose provided result on autocomplete', async function () {// 
    widgetsPOM.choseProvidedResult();
    await browser.sleep(4000);
});