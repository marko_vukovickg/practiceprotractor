var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let loginPOM = require('../pages/loginPOM.js');


When('User enters the following email: {string} and password {string}', async function (email, password) {
    loginPOM.enterUsernameAndPass(email, password);
    await browser.sleep(2000);
});


When('User clicks login button', async function () {
    loginPOM.clickSignInBtn();
    await browser.sleep(2000);
});


Then('User should see error message {string}', async function (errorMsg) {
    let msg =  loginPOM.checkIfErrorMessagePresent();
    await msg.then(function (actual) {
        expect(actual).to.equal(errorMsg, 'Expected is:' + errorMsg + ' Actual is:' + actual);
    });
    await browser.sleep(2000);
});