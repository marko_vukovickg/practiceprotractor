Feature: Switch to alerts and perform operations


    @smoke
        Scenario: Test alert with ok and cancel
            Given User navigates to url: "http://demo.automationtesting.in/Alerts.html"
            When User slects alert with OK button
            Then User clicks to show alert
            Then User clicks on OK button on alert
            Then User should see message "You pressed Ok"


    @smoke
    Scenario Outline: Test alert with text box
        Given User navigates to url: "http://demo.automationtesting.in/Alerts.html"
        When User slects alert with text box
        Then User clicks to show alert with text box
        Then User enters "<name>" in text box
        Then User clicks on OK button on alert
        Then User should see greeting message "Hello" "<name>" "How are you today"


        Examples:
            | name  |
            | Marko |