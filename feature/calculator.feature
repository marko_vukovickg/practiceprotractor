Feature: Calculator app Usage


    Scenario Outline: Test different operations for two fiven numbers
        Given User navigates to url: "http://juliemr.github.io/protractor-demo/"
        When User enters first number "<firstNumber>" and second "<secondNumber>"
        Then User selects operation "<operation>" to be performed
        Then User select submit button
        Then User verify the result to be "<result>"


        @regression
        Examples:
            | firstNumber | secondNumber | operation | result |
            | 6           | 8            | +         | 14     |
            | 10          | 7            | -         | 3      |
            | 2           | 5            | *         | 10     |
            | 8           | 2            | /         | 4      |