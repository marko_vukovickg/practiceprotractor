Feature: Test capabilities of widgets


    @regression
    Scenario Outline: Send date to disabled datePicker via JS executor
        Given User navigates to url: "http://demo.automationtesting.in/Datepicker.html"
        When User selects date picker disabled widget
        Then User enters the date "<date>" on disabled widget

        Examples:
            | date       |
            | 04/30/2020 |


    @smoke
    Scenario Outline: Send date to enabled datePicker
        Given User navigates to url: "http://demo.automationtesting.in/Datepicker.html"
        When User selects date picker enabled widget
        Then User enters the date "<date>" on enabled widget

        Examples:
            | date       |
            | 04/30/2020 |


    @regression
    Scenario Outline: Chose date on disabled datePicker - complicated
        Given User navigates to url: "http://demo.automationtesting.in/Datepicker.html"
        When User selects date picker disabled widget
        Then User chose the date "<date>" on disabled widget

        Examples:
            | date       |
            | 09/08/2020 |
            | 09/08/2021 |


    @test
    Scenario Outline: Chose provided result for autocomplete search
        Given User navigates to url: "http://demo.automationtesting.in/AutoComplete.html"
        Then User enters search parameter "<searchData>"
        Then User chose provided result on autocomplete

        Examples:
            | searchData  |
            | New Zealand |
            | Serbia      |
            | Montenegro  |
