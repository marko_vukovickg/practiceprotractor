Feature: Login form fill


@smoke
    Scenario: Fill Log in form with wrong data
        Given User navigates to url: "http://demo.automationtesting.in/SignIn.html"
        When User enters the following email: "test@test.com" and password "password"
        Then User clicks login button
        Then User should see error message "Invalid User Name or PassWord"