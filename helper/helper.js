// import { browser } from "protractor";

var helper = function () {

    this.waitForElementPresent = function (element) {
        browser.wait(() => element.isPresent(), 60000);
    }

    this.waitForElementDisplayed = function (element) {
        browser.wait(() => element.isDisplayed(), 60000);
    }

    this.sleep = function (time) {
        browser.sleep(time);
    }

    this.getClassAttribute = function (element) {
        return element.getAttribute('class');
    }

    this.getAttributeValue = function (element) {
        return element.getAttribute('value');
    }

    this.getText = function (element) {
        return element.getText();
    }

    this.findElementsByOptionText = function (condition) {
        return element.all(by.cssContainingText('option', condition))
    }

    this.findElementByOptionText = function (condition) {
        return element(by.cssContainingText('option', condition))
    }

    this.findElementByLinkText = function (condition) {
        return element(by.cssContainingText('a', condition))
    }

    this.findElementBySpanText = function (condition) {
        return element(by.cssContainingText('span', condition))
    }

}

module.exports = new helper();